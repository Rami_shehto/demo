package rami.test;

import java.util.List;


public class TwoMAStrategy
{

    int ticker;
    int s;
    int l;
    
    public TwoMAStrategy(int ticker, int s, int l) {
        this.ticker = ticker;
        this.s = s;
        this.l = l;
    }
    
    public static void main(String[] args) {
        
        double shortAverage;
        double longAverage;
        boolean shortIsBigger = true;
        //boolean longIsBigger = false;
        
        boolean position = true;
 
        TwoMAStrategy strategy = new TwoMAStrategy(2000, 4, 10);
        
        
        while (true){
        MovingAverage ma = new MovingAverage();
        ma.instantiate ();
        List<Double> list = ma.getList ();
        
        shortAverage = ma.getAverage (4, list);
        System.out.println ("Short Avg = " + shortAverage);
        

        longAverage = ma.getAverage (10, list);
        System.out.println ("Long Avg = " +longAverage);

        
        //update which is bigger
        if (shortAverage > longAverage){
            
            //buy
            shortIsBigger = true;
            //longIsBigger = false;
            
            //System.out.println ("buy");
        }
        
        else if (shortAverage < longAverage){
            
            //sell
            
            shortIsBigger = false;
            //longIsBigger = true;
            
            //System.out.println ("sell");

        }

        //check if cross happened
        if (shortIsBigger != position){
            
            
            System.out.println ("Cross!");

            
            if (shortAverage > longAverage){
                
                //buy
                shortIsBigger = true;
                longIsBigger = false;
                
                System.out.println ("Short is bigger, buy!");
            }
            
            else if (shortAverage < longAverage){
                
                //sell
                
                shortIsBigger = false;
                longIsBigger = true;
                
                System.out.println ("Long is bigger, sell!");
    
            }
        }
        
        
        position = shortIsBigger;
        ma.updateValue ();
        
        
        
        try {
            Thread.sleep(5000);                 //1000 milliseconds is one second.
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }    }
    }
    
}
